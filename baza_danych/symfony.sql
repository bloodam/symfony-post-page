-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 13 Mar 2018, 14:37
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `symfony`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_description` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `long_description` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` datetime NOT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Zrzut danych tabeli `blog`
--

INSERT INTO `blog` (`id`, `title`, `short_description`, `long_description`, `create_date`, `image_url`) VALUES
(1, 'Post nr 1 - edycja', 'Opis skrócony postu nr 1 po poprawce', 'Długi opis postu nr 1 Długi opis postu nr 1 Długi opis postu nr 1Długi opis postu nr 1Długi opis postu nr 1Długi opis postu nr 1Długi opis postu nr 1Długi opis postu nr 1Długi opis postu nr 1', '2018-03-09 06:19:00', '../../../../images/sympfony.jpeg'),
(2, 'Post nr 2', 'Krótki opis postu nr 2', 'Długi opis postu nr 2 Długi opis postu nr 2 Długi opis postu nr 2 Długi opis postu nr 2 Długi opis postu nr 2 Długi opis postu nr 2 Długi opis postu nr 2 Długi opis postu nr 2 Długi opis postu nr 2 Długi opis postu nr 2', '2018-03-09 08:24:00', '../../../../images/sympfony.jpeg'),
(3, 'Post 3', 'Krótki opis postu 3', 'Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3 Długi opis postu 3', '2015-01-01 00:00:00', '../../../../images/sympfony.jpeg'),
(10, 'POst 4 z obrazkiem', 'post z możliwością dodania obrazka', 'Dodanie obrazka', '2017-01-01 00:00:00', '/images/84a4e3a7cefdc7b56365402c911ebd06.jpeg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Nowość'),
(2, 'Sport'),
(3, 'Muzyka'),
(4, 'Wiadomości');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `category_relation`
--

CREATE TABLE IF NOT EXISTS `category_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_post` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_post` int(11) NOT NULL,
  `content` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
