<?php

namespace AppBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use AppBundle\Entity\Blog;
use AppBundle\Entity\Category;
use AppBundle\Entity\CategoryRelation;
use AppBundle\Entity\Comments;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BlogController extends Controller
{
    /**
     * @Route("/blog", name="blog_list")
     */
    public function listAction(Request $request)
    {
        $posts = $this->getDoctrine()
                 ->getRepository('AppBundle:Blog')
                 ->findAll();
        return $this->render('blog/index.html.twig', array('posts'=>$posts));
    }

    /**
     * @Route("/blog/create", name="blog_create")
     */
    public function createAction(Request $request)
    {

        $post = new Blog();
        $form = $this->createFormBuilder($post)
                ->add('title', TextType::class, array('label' => 'Tytuł', 'attr' => array('class' => 'form-control', 'style' => 'margin: 20px')))
                ->add('short_description', TextareaType::class, array('label' => 'Krótki opis','attr' => array('class' => 'form-control', 'style' => 'margin: 20px')))
                ->add('long_description', TextareaType::class, array('label' => 'Długi opis','attr' => array('class' => 'form-control', 'style' => 'margin: 20px')))
                ->add('create_date', DateTimeType::class, array('label' => 'Data utworzenia','attr' => array('class' => 'form-date', 'style' => 'margin: 20px')))
                ->add('image_url', FileType::class, array('label' => 'Dodaj obrazek','attr' => array('class' => 'form-control', 'style' => 'margin: 20px')))
                ->add('save', SubmitType::class, array('label' => 'Zapisz post', 'attr' => array('class' => 'btn btn-success', 'style' => 'margin: 20px')))
                -> getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $title = $form['title']->getData();
            $short_description = $form['short_description']->getData();
            $long_description = $form['long_description']->getData();
            $create_date = $form['create_date']->getData();
            $image_url = $form['image_url']->getData();
            $file = $post->getImageUrl();
            $file_name = md5(uniqid()).'.jpeg';
            $image_url->move($this->getParameter('image_dir'), $file_name);

            $post->setTitle($title);
            $post->setLongDescription($long_description);
            $post->setShortDescription($short_description);
            $post->setCreateDate($create_date);
            $post->setImageUrl("/images/".$file_name);

            $p = $this->getDoctrine()->getManager();
            $p->persist($post);
            $p->flush();

            $this->addFlash(
                'notice',
                'Post dodano'
            );
            return $this->redirectToRoute('blog_list');
        }
        return $this->render('blog/create.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/blog/edit/{id}", name="blog_edit")
     */
    public function editAction($id, Request $request)
    {
      $post = $this->getDoctrine()
              ->getRepository('AppBundle:Blog')
              ->find($id);

              $post->setTitle($post->getTitle());
              $post->setLongDescription($post->getLongDescription());
              $post->setShortDescription($post->getShortDescription());
              $post->setCreateDate($post->getCreateDate());
              $post->setImageUrl($post->getImageUrl());

              $form = $this->createFormBuilder($post)
                      ->add('title', TextType::class, array('label' => 'Tytuł', 'attr' => array('class' => 'form-control', 'style' => 'margin: 20px')))
                      ->add('short_description', TextareaType::class, array('label' => 'Krótki opis','attr' => array('class' => 'form-control', 'style' => 'margin: 20px')))
                      ->add('long_description', TextareaType::class, array('label' => 'Długi opis','attr' => array('class' => 'form-control', 'style' => 'margin: 20px')))
                      ->add('create_date', DateTimeType::class, array('label' => 'Data utworzenia','attr' => array('class' => 'form-date', 'style' => 'margin: 20px')))
                      ->add('image_url', TextType::class, array('label' => 'URL obrazka','attr' => array('class' => 'form-control', 'style' => 'margin: 20px')))
                      ->add('save', SubmitType::class, array('label' => 'Zapisz zmiany', 'attr' => array('class' => 'btn btn-success', 'style' => 'margin: 20px')))
                      -> getForm();
              $form->handleRequest($request);
              if($form->isSubmitted() && $form->isValid()){
                  $title = $form['title']->getData();
                  $short_description = $form['short_description']->getData();
                  $long_description = $form['long_description']->getData();
                  $create_date = $form['create_date']->getData();
                  $image_url = $form['image_url']->getData();

                  $p = $this->getDoctrine()->getManager();
                  $post = $p->getRepository('AppBundle:Blog')->find($id);

                  $post->setTitle($title);
                  $post->setLongDescription($long_description);
                  $post->setShortDescription($short_description);
                  $post->setCreateDate($create_date);
                  $post->setImageUrl($image_url);

                  $p->flush();

                  $this->addFlash(
                      'notice',
                      'Post zaktualizowano'
                  );
                  return $this->redirectToRoute('blog_list');
              }


      return $this->render('blog/edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/blog/details/{id}", name="blog_details")
     */
    public function detailsAction($id)
    {
        $post = $this->getDoctrine()
                ->getRepository('AppBundle:Blog')
                ->find($id);

        return $this->render('blog/details.html.twig', array('post' => $post));
    }

    /**
     * @Route("/blog/delete/{id}", name="blog_delete")
     */
    public function deleteAction($id)
    {
        $p = $this->getDoctrine()->getManager();
        $post = $p->getRepository('AppBundle:Blog')->find($id);

        $p->remove($post);
        $p->flush();
        $this->addFlash(
          'notice',
          'Post usunięto'
        );

        return $this->redirectToRoute('blog_list');
    }
}
